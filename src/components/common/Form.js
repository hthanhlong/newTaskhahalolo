import React from "react";
import InputField from "./InputField";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import { makeStyles } from "@material-ui/core/styles";
import CustomMenus from "./CustomMenus";
import SelectedControl from "./SelectedControl";
import MyList from "./MyList";
import WrapButton from "./WrapButton";

const Form = (props) => {
  const {
    dataInputField,
    timeInput,
    describeInput,
    handleCloseForm,
    labelDescInput,
    descInputPlaceholder,
  } = props;

  const handleCloseBigForm = () => {
    handleCloseForm();
  };

  const classes = useStyles();

  return (
    <form>
      <div className={classes.container}>
        <MyList
          data={dataInputField || []}
          render={(item) => (
            <InputField label={item.label} placeholder={item.placeholder} />
          )}
        />
        {describeInput && (
          <div style={{ display: "flex" }}>
            <label style={{ flex: 0.2, marginRight: "0.3rem" }}>
              {labelDescInput}
            </label>
            <TextareaAutosize
              aria-label="empty textarea"
              placeholder={descInputPlaceholder}
              rowsMin={3}
              className={classes.area}
            />
          </div>
        )}
        {timeInput && (
          <div style={{ display: "flex" }}>
            <label style={{ flex: 0.2, marginTop: "0.5rem" }}>
              Khoảng Thời gian
            </label>
            <div style={{ flex: 0.8 }}>
              <div>
                <FormControlLabel
                  control={<Checkbox name="checkedB" color="primary" />}
                  label="Tôi hiện đang làm việc tại đây"
                />
              </div>
              <div>
                <SelectedControl />
                <SelectedControl />
                <SelectedControl />
              </div>
              <div style={{ margin: "0.5rem 0" }}>Đến</div>
              <div>
                <SelectedControl />
                <SelectedControl />
                <SelectedControl />
              </div>
            </div>
          </div>
        )}
        <Divider style={{ margin: "1rem 0" }} />
        <WrapButton
          isShowCustomMenus={true}
          handleCancelForm={handleCloseBigForm}
        />
      </div>
    </form>
  );
};

const useStyles = makeStyles({
  container: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: "#eaf1f7",
    padding: "1rem",
    margin: "1rem 0",
    justifyContent: "center",
  },
  name1: {
    width: "7rem",
    padding: "0.5rem 1rem",
  },

  area: {
    width: "100%",
    borderColor: "#CFCDCD",
    flex: 0.8,
    borderRadius: "0.2rem",
    padding: "0.5rem",
    fontSize: "1rem",
  },
  btnSave: {
    margin: "0 1rem",
    backgroundColor: "blue !important",
    color: "white",
    outlineStyle: "none !important",
  },
});

export default Form;
