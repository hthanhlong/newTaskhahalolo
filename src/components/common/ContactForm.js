import React from "react";
import { Grid, Divider, List, ListItem, Button } from "@material-ui/core";
import WrapButton from "./WrapButton";
import CustomMenus from "./CustomMenus";
import SelectedControl from "./SelectedControl";
import { makeStyles } from "@material-ui/core/styles";
import OutlinedInput from "@material-ui/core/OutlinedInput";

const ContactForm = (props) => {
  const { title, listInfo, titleBtn, placeholder, isShowForm } = props;

  const classes = useStyles();

  return (
    <form>
      <Grid
        container
        style={{
          backgroundColor: "#EAF1F7",
          margin: "1rem 0",
        }}
        direction="column"
      >
        <Grid container spacing={2}>
          <Grid item lg={3} style={{ textAlign: "end", marginTop: "1.3rem" }}>
            {title}
          </Grid>
          <Grid item lg={9}>
            <List>
              {listInfo.map((item, index) => (
                <ListItem key={index}>
                  <div>{item.info}</div>
                  <div style={{ margin: "0 1rem" }}>
                    <CustomMenus />
                  </div>
                  <div style={{ color: "red" }}>Remove</div>
                </ListItem>
              ))}
            </List>

            <div style={{ display: "flex", marginLeft: "0.85rem" }}>
              {isShowForm && (
                <div>
                  <SelectedControl />
                </div>
              )}
              <OutlinedInput
                placeholder={placeholder}
                fullWidth
                className={classes.root}
              />
            </div>

            <div>
              <Button className={classes.btn}>{titleBtn}</Button>
            </div>
          </Grid>
        </Grid>
      </Grid>
    </form>
  );
};

const useStyles = makeStyles({
  root: {
    backgroundColor: "white",
    height: "2.2rem",
  },
  btn: {
    backgroundColor: "#5A6268",
    marginLeft: "0.9rem",
    marginTop: "1rem",
    color: "white",
    textTransform: "none",
    "&:hover": {
      backgroundColor: "#212121",
    },
  },
});

export default ContactForm;
