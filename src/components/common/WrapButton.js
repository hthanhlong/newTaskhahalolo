import React from "react";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import CustomMenus from "./CustomMenus";

const WrapButton = (props) => {
  const { isShowCustomMenus, handleCancelForm } = props;

  const handleForm = () => {
    handleCancelForm();
  };

  const classes = useStyles();
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "flex-end",
      }}
    >
      {isShowCustomMenus && <CustomMenus />}
      <Button className={classes.btnSave} variant="outlined">
        Save
      </Button>
      <Button
        onClick={handleForm}
        variant="outlined"
        style={{ height: "2rem" }}
      >
        Cancel
      </Button>
    </div>
  );
};
const useStyles = makeStyles({
  btnSave: {
    margin: "0 1rem",
    backgroundColor: "blue !important",
    color: "white",
    outlineStyle: "none !important",
    height: "2rem",
  },
});

export default WrapButton;
