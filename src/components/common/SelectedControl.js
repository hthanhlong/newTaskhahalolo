import React from "react";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import { makeStyles } from "@material-ui/core/styles";

const SelectedControl = () => {
  const classes = useStyles();
  return (
    <FormControl
      variant="outlined"
      focused={false}
      style={{ marginRight: "1.5rem" }}
    >
      <Select
        native
        value={2020}
        inputProps={{
          name: "age",
          id: "outlined-age-native-simple",
          classes: {
            root: classes.name1,
          },
        }}
      >
        <option value={2020}>2020</option>
        <option value={2021}>2021</option>
      </Select>
    </FormControl>
  );
};

const useStyles = makeStyles({
  name1: {
    width: "7rem",
    padding: "0.5rem 1rem",
    backgroundColor: "white",
  },
});

export default SelectedControl;
