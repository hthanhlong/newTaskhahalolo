import React from "react";
import ErrorOutlineIcon from "@material-ui/icons/ErrorOutline";
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import OutlinedInput from "@material-ui/core/OutlinedInput";

const InputField = (props) => {
  const { label, placeholder, multiline, rows } = props;

  const classes = useStyles();
  return (
    <div className={classes.container}>
      <label className={classes.label}>{label}</label>
      <div className={classes.wrapInput}>
        <OutlinedInput
          className={classes.input}
          variant="outlined"
          placeholder={placeholder}
          fullWidth
          error={false}
          endAdornment={
            <InputAdornment position="end">
              <ErrorOutlineIcon />
            </InputAdornment>
          }
        />
      </div>
    </div>
  );
};

const useStyles = makeStyles({
  root: {
    backgroundColor: "red",
  },
  container: {
    display: "flex",
    alignItems: "center",
    marginBottom: "1rem",
  },
  label: {
    flex: 0.2,
    margin: "auto 0",
  },
  wrapInput: {
    display: "flex",
    flex: 0.8,
    alignItems: "center",
  },
  input: {
    backgroundColor: "white",
    height: "2.5rem",
  },
});

export default InputField;
