import React, { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import PublicIcon from "@material-ui/icons/Public";
import PeopleIcon from "@material-ui/icons/People";
import PersonIcon from "@material-ui/icons/Person";
import PersonAddDisabledIcon from "@material-ui/icons/PersonAddDisabled";
import LockIcon from "@material-ui/icons/Lock";
import SettingsIcon from "@material-ui/icons/Settings";

export default function CustomMenus() {
  const dataList = [
    {
      id: 1,
      icon: <PublicIcon />,
      text: "Công khai",
    },
    {
      id: 2,
      icon: <PeopleIcon />,
      text: "Bạn của bạn bè",
    },
    {
      id: 3,
      icon: <PersonAddDisabledIcon />,
      text: "Bạn bè ngoại trừ...",
    },
    {
      id: 4,
      icon: <PersonIcon />,
      text: "Bạn bè cụ thể",
    },
    {
      id: 5,
      icon: <LockIcon />,
      text: "Chỉ mình tôi",
    },
    {
      id: 6,
      icon: <SettingsIcon />,
      text: "Tùy chỉnh",
    },
  ];

  const [anchorEl, setAnchorEl] = useState(null);

  const [isSelected, setIsSelected] = useState(dataList[0]);

  const handleChangeContent = (item) => {
    setIsSelected(item);
    setAnchorEl(null);
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <Button
        aria-controls="customized-menu"
        aria-haspopup="true"
        onClick={handleClick}
        style={{
          border: "1px solid black",
          display: "flex",
          justifyContent: "space-between",
          height: "2rem",
          backgroundColor: "#EAF1F7",
        }}
      >
        <ListItemIcon style={{ minWidth: "2rem" }}>
          {isSelected.icon}
        </ListItemIcon>
        <ListItemText
          primary={isSelected.text}
          style={{ textTransform: "none" }}
        />
        <ArrowDropDownIcon />
      </Button>
      <StyledMenu
        id="customized-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        style={{ width: "15rem" }}
      >
        {dataList &&
          dataList.map((item, index) => (
            <StyledMenuItem
              key={index}
              onClick={() => handleChangeContent(item)}
            >
              <ListItemIcon style={{ minWidth: "2rem" }}>
                {item.icon}
              </ListItemIcon>
              <ListItemText primary={item.text} />
            </StyledMenuItem>
          ))}
      </StyledMenu>
    </div>
  );
}

const StyledMenuItem = withStyles((theme) => ({
  root: {
    height: "2rem",
  },
}))(MenuItem);

const StyledMenu = withStyles({
  paper: {
    border: "1px solid #d3d4d5",
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "center",
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "center",
    }}
    {...props}
  />
));
