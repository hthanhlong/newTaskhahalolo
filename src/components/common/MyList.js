import React from "react";

const MyList = (props) => {
  const { data, render } = props;

  if (!data) return "";

  return <div>{data.map((item, index) => render(item, index))}</div>;
};

export default MyList;
