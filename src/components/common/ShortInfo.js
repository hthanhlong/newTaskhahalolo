import React, { useState } from "react";
import { Typography, CardMedia, Menu, MenuItem } from "@material-ui/core";
import PublicIcon from "@material-ui/icons/Public";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import xe1 from "../../assets/images/xe1.png";
import { makeStyles } from "@material-ui/core/styles";

const ShortInfo = (props) => {
  const { img, title, desc } = props;

  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick1 = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const classes = useStyles();
  return (
    <div className={classes.container}>
      <div className={classes.wrapContent}>
        <CardMedia image={xe1} className={classes.image} />

        <div>
          <Typography style={{ cursor: "pointer" }}>{title}</Typography>
          <Typography className={classes.descrip}>{desc}</Typography>
        </div>
      </div>
      <div className={classes.shortIcons}>
        <PublicIcon
          fontSize="small"
          style={{ marginRight: "0.5rem", cursor: "pointer" }}
        />
        <MoreHorizIcon
          fontSize="small"
          style={{ cursor: "pointer" }}
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleClick1}
        />
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <MenuItem onClick={handleClose}>Sửa</MenuItem>
          <MenuItem onClick={handleClose}>Hủy</MenuItem>
        </Menu>
      </div>
    </div>
  );
};

const useStyles = makeStyles({
  container: {
    display: "flex",
    justifyContent: "space-between",
    padding: "1rem 0",
    borderTop: " 1px solid #CFCDCD",
  },
  image: {
    width: 45,
    height: 45,
    borderRadius: "50%",
    marginRight: "1rem",
  },
  wrapContent: {
    display: "flex",
  },
  descrip: {
    fontSize: "0.8rem",
    color: "#6C757E",
  },
  shortIcons: {
    display: "flex",
    alignItems: "center",
  },
});

export default ShortInfo;
