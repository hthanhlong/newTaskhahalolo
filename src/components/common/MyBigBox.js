import React, { useState } from "react";
import { Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import ShortInfo from "./ShortInfo";
import AddIcon from "@material-ui/icons/Add";
import Form from "./Form";
import MyList from "./MyList";

const MyBigBox = (props) => {
  const {
    describeInput,
    timeInput,
    dataInputField,
    listData,
    title,
    labelAddInput,
    labelDescInput,
    descInputPlaceholder,
  } = props;

  const [isShowForm, setIsShowForm] = useState(false);
  const [isShowInputField, setIsInputField] = useState(true);

  const handleShowTable = () => {
    setIsShowForm(true);
    setIsInputField(false);
  };

  const handleCloseForm = () => {
    setIsShowForm(false);
    setIsInputField(true);
  };

  const classes = useStyles();
  return (
    <Grid item style={{ marginTop: "1rem" }}>
      <Typography style={{ textTransform: "uppercase" }}>{title}</Typography>

      {isShowInputField && (
        <div className={classes.inputText} onClick={handleShowTable}>
          <AddIcon className={classes.addIcons} />
          <Typography>{labelAddInput}</Typography>
        </div>
      )}
      {isShowForm && (
        <Form
          dataInputField={dataInputField}
          timeInput={timeInput}
          describeInput={describeInput}
          handleCloseForm={handleCloseForm}
          labelDescInput={labelDescInput}
          descInputPlaceholder={descInputPlaceholder}
        />
      )}
      <MyList
        data={listData}
        render={(item, index) => (
          <ShortInfo key={index} title={item.title} desc={item.desc} />
        )}
      />
    </Grid>
  );
};

const useStyles = makeStyles((theme) => ({
  inputText: {
    display: "flex",
    alignItems: "center",
    margin: "1rem 0",
    backgroundColor: "#F1F4F9",
    padding: "0.6rem 0.5rem",
    borderRadius: "0.3rem",
    borderWidth: 1,
    borderStyle: "dashed",
    borderColor: "#6C757E",
    cursor: "pointer",
    height: "100%",
    "&:hover": {
      backgroundColor: "#e5effc",
    },
  },
  addIcons: {
    marginRight: "0.5rem",
    fontWeight: 700,
  },
}));

export default MyBigBox;
