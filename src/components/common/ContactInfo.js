import React, { useState } from "react";
import {
  Grid,
  List,
  ListItem,
  ListItemText,
  Divider,
  ListItemIcon,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import PublicIcon from "@material-ui/icons/Public";
import WrapButton from "./WrapButton";

const ContactInfo = (props) => {
  //
  const { label, listInfo, iconPublic, form, icon } = props;

  const [isShowForm, setIsShowForm] = useState(false);

  const handleFixButton = () => {
    setIsShowForm(true);
  };

  const handleCancelForm = () => {
    setIsShowForm(false);
  };

  const classes = useStyles();
  return (
    <Grid>
      {!isShowForm && (
        <Grid
          container
          style={{
            marginBottom: "1rem",
            borderTop: "1px solid #dddddd",
          }}
        >
          <Grid item xs={12} lg={listInfo ? 3 : 9}>
            <List style={{ display: "flex", alignContent: "center" }}>
              <ListItem
                style={{
                  padding: "0",
                }}
              >
                {icon && <ListItemIcon>{icon}</ListItemIcon>}
                <ListItemText primary={label} />
              </ListItem>
            </List>
          </Grid>
          {listInfo && (
            <Grid item xs={12} lg={6}>
              <List>
                {listInfo &&
                  listInfo.map((item, index) => (
                    <ListItem
                      key={index}
                      style={{
                        padding: "0 1rem",
                        display: "flex",
                        alignItems: "center",
                      }}
                    >
                      <div>
                        <ListItemText
                          primary={item.info}
                          style={{ marginRight: "1rem" }}
                        />
                      </div>
                      {item.icon}
                    </ListItem>
                  ))}
              </List>
            </Grid>
          )}
          <Grid item xs={12} lg={3}>
            <List>
              <ListItem
                onClick={handleFixButton}
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  padding: "0.1rem 1rem",
                }}
              >
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  {iconPublic && <PublicIcon />}
                  <ListItemText className={classes.fixBtn} primary="Sửa" />
                </div>
              </ListItem>
            </List>
          </Grid>
        </Grid>
      )}
      <Grid>
        {isShowForm && (
          <div
            style={{
              backgroundColor: "#EAF1F7",
              padding: "1rem",
              marginBottom: "1rem",
            }}
          >
            {form}
            <Divider style={{ margin: "1rem 0" }} />
            <Grid item style={{ textAlign: "end" }}>
              <WrapButton handleCancelForm={handleCancelForm} />
            </Grid>
          </div>
        )}
      </Grid>
    </Grid>
  );
};

const useStyles = makeStyles({
  fixBtn: {
    marginLeft: "0.5rem",
    cursor: "pointer",
    "&:hover": {
      color: "red",
    },
  },
});

export default ContactInfo;
