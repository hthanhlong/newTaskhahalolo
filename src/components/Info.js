import React from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import SmartphoneIcon from "@material-ui/icons/Smartphone";
import CakeIcon from "@material-ui/icons/Cake";
import WcIcon from "@material-ui/icons/Wc";
import { Typography } from "@material-ui/core";
import "./info.css";

const Info = () => {
  const demo = [
    {
      icons: <SmartphoneIcon />,
      title: "0978 132465",
    },
    {
      icons: <CakeIcon />,
      title: "25 Tháng 10, 1993",
    },
    {
      icons: <WcIcon />,
      title: "Nam",
    },
  ];

  return (
    <div>
      <List>
        {demo &&
          demo.map((item, index) => (
            <ListItem key={index}>
              {item.icons}
              <Typography style={{ marginLeft: "1rem" }}>
                {item.title}
              </Typography>
            </ListItem>
          ))}
      </List>
      <span className="fixbutton">Sửa</span>
    </div>
  );
};

export default Info;
