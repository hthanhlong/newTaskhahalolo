import React, { useState } from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Typography from "@material-ui/core/Typography";
import "./menu.css";
import { makeStyles } from "@material-ui/core/styles";

const Menu = (props) => {
  const { handleSelectedFromApp } = props;

  const [isActive, setIsActive] = useState(1);

  const list = [
    {
      id: 1,
      title: "Tổng Quan",
    },
    {
      id: 2,
      title: "Công việc và học vấn",
    },
    {
      id: 3,
      title: "Nơi từng sống",
    },
    {
      id: 4,
      title: "Thông tin liên hệ và cơ bản",
    },
    {
      id: 5,
      title: "Gia đình và các mối quan hệ",
    },
    {
      id: 6,
      title: "Chi tiết về bạn",
    },
    {
      id: 7,
      title: "Sự kiện trong đời",
    },
  ];

  const handleSelected = (item) => {
    handleSelectedFromApp(item);
    setIsActive(item.id);
  };

  const classes = useStyles();

  return (
    <div className="menu">
      <List>
        {list &&
          list.map((item, index) => (
            <ListItem
              button
              key={index}
              className={classes.listItem}
              onClick={() => handleSelected(item)}
            >
              <Typography
                className={isActive === item.id ? `${classes.active}` : ""}
              >
                {item.title}
              </Typography>
            </ListItem>
          ))}
      </List>
    </div>
  );
};

const useStyles = makeStyles((theme) => ({
  listItem: {
    borderBottom: "1px solid black",
  },
  "&:last-child": {
    borderBottom: "none",
  },
  active: {
    color: "red",
  },
}));

export default Menu;
