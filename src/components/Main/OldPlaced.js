import React from "react";
import { Grid } from "@material-ui/core";
import MyBigBox from "../common/MyBigBox";
import Divider from "@material-ui/core/Divider";

const OldPlaced = () => {
  const cityInput = [
    {
      label: "Thành phố hiện tại",
    },
  ];
  const townInput = [
    {
      label: "Quê quán",
    },
  ];

  const cityList = [
    {
      title: "Ho chi Minh, Viet Nam",
      desc: "Tỉnh/Thành Phố hiện tại",
    },
    {
      title: "Hà Nội",
      desc: "Tỉnh/Thành Phố hiện tại",
    },
    {
      title: "Huế",
      desc: "Tỉnh/Thành Phố hiện tại",
    },
  ];

  return (
    <div>
      <MyBigBox
        dataInputField={cityInput}
        title="THÀNH PHỐ HIỆN TẠI VÀ QUÊ HƯƠNG"
        labelAddInput="Thêm thành phố hiện tại của bạn"
      />
      <Divider />
      <MyBigBox
        dataInputField={townInput}
        listData={cityList}
        labelAddInput="Thêm quê quán của bạn"
      />
    </div>
  );
};

export default OldPlaced;
