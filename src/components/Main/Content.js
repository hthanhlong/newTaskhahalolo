import React from "react";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import xe1 from "../../assets/images/xe1.png";
import xe2 from "../../assets/images/xe2.png";
import "./content.css";
import Info from "../Info";

const Content = () => {
  return (
    <>
      <div className="containerInfo">
        <div className="wrappcontent">
          <div className="content">
            <div className="left_content">
              <div className="content_avatar">
                <img src={xe1} alt="xe1123" />
              </div>
              <div className="content_text">
                <span className="content_title">
                  Làm <a href="#">Dev</a> tại Hahalolo và
                  <a href="#"> Làm biếng ở Nhà</a>
                </span>
                <span className="content_desc">
                  Quá khứ: <a href="#">Halotimes</a> và <a href="#">Teen1s</a>
                </span>
              </div>
            </div>
            <div className="right_content">
              <MoreHorizIcon style={{ cursor: "pointer" }} />
            </div>
          </div>
          <div className="content">
            <div className="left_content">
              <div className="content_avatar">
                <img src={xe2} alt="xe1123" />
              </div>
              <div className="content_text">
                <span className="content_title">
                  Làm <a href="#">Dev</a> tại Hahalolo và
                  <a href="#"> Làm biếng ở Nhà</a>
                </span>
                <span className="content_desc">
                  Quá khứ: <a href="#">Halotimes</a> và <a href="#">Teen1s</a>
                </span>
              </div>
            </div>
            <div className="right_content">
              <MoreHorizIcon style={{ cursor: "pointer" }} />
            </div>
          </div>
          <div className="content">
            <div className="left_content">
              <div className="content_avatar">
                <img src={xe1} alt="xe1123" />
              </div>
              <div className="content_text">
                <span className="content_title">
                  Làm <a href="#">Dev</a> tại Hahalolo và
                  <a href="#"> Làm biếng ở Nhà</a>
                </span>
                <span className="content_desc">
                  Quá khứ: <a href="#">Halotimes</a> và <a href="#">Teen1s</a>
                </span>
              </div>
            </div>
            <div className="right_content">
              <MoreHorizIcon style={{ cursor: "pointer" }} />
            </div>
          </div>
        </div>
        <div>
          <Info />
        </div>
      </div>
    </>
  );
};

export default Content;
