import React from "react";
import { Grid, Typography } from "@material-ui/core";
import MyBigBox from "../common/MyBigBox";
import ContactInfo from "../common/ContactInfo";
import FavoriteIcon from "@material-ui/icons/Favorite";

const Family = () => {
  const dataInput = [
    {
      label: "Thành viên trong gia đình",
      placeholder: "Nhập thêm thành viên",
    },
  ];

  const familyData = [
    {
      title: "Vợ là của em",
      desc: "Vợ",
    },
    {
      title: "Abc",
      desc: "Em gái",
    },
    {
      title: "Cdf",
      desc: "Con trai",
    },
  ];

  return (
    <Grid container direction="column">
      <Grid item>
        <Typography>TÌNH TRẠNG MỐI QUAN HỆ</Typography>
        <Grid>
          <ContactInfo
            label="Độc thân"
            icon={<FavoriteIcon />}
            iconPublic={true}
          />
        </Grid>
      </Grid>
      <MyBigBox
        dataInputField={dataInput}
        describeInput={false}
        timeInput={false}
        listData={familyData}
        title="THÀNH VIÊN TRONG GIA ĐÌNH"
        labelAddInput="Thêm một thành viên gia đình"
      />
    </Grid>
  );
};

export default Family;
