import React from "react";
import { Grid } from "@material-ui/core";
import MyBigBox from "../common/MyBigBox";

const WorkAndLife = () => {
  const jobDataInput = [
    {
      label: "Công ty",
      placeholder: "Tên công ty",
    },
    {
      label: "Chức vụ",
      placeholder: "Chức vụ của bạn",
    },
    {
      label: "Thành Phố/Tỉnh",
      placeholder: "Nhập địa điểm",
    },
  ];

  const jobDataList = [
    {
      title: "IT1",
      desc: "Dev from 2016",
    },
    {
      title: "IT2",
      desc: "Dev from 2017",
    },
    {
      title: "IT3",
      desc: "Dev from 2018",
    },
  ];

  const LevelSchoolInput = [
    {
      label: "Trường học",
      placeholder: "Tên trường học",
    },
    {
      label: "Thành Phố/Tỉnh",
      placeholder: "Nhập địa điểm",
    },
  ];

  const levelSchoolList = [
    {
      title: "FPT",
      desc: "Dev from 2016",
    },
    {
      title: "BachKhoa",
      desc: "Dev from 2017",
    },
    {
      title: "Hue",
      desc: "Dev from 2018",
    },
  ];
  return (
    <Grid container direction="column">
      <MyBigBox
        dataInputField={jobDataInput}
        describeInput={true}
        timeInput={true}
        listData={jobDataList}
        title="Công Việc"
        labelDescInput="Mô tả"
        descInputPlaceholder="Mô tả về công việc"
        labelAddInput="Thêm công việc"
      />
      <MyBigBox
        dataInputField={LevelSchoolInput}
        describeInput={true}
        timeInput={true}
        listData={levelSchoolList}
        title="Học Vấn"
        labelDescInput="Mô tả"
        descInputPlaceholder="Mô tả về công việc"
        labelAddInput="Thêm học vấn"
      />
    </Grid>
  );
};

export default WorkAndLife;
