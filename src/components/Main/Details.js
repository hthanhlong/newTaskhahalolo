import React from "react";
import { Grid } from "@material-ui/core";
import MyBigBox from "../common/MyBigBox";
import ContactInfo from "../common/ContactInfo";

const Details = () => {
  const otherName = [
    {
      label: "Tên",
      placeholder: "Biệt danh của bạn là gì?",
    },
  ];
  const levelSchoolList = [
    {
      title: "UI",
      desc: "Dũng sĩ diệt mồi",
    },
    {
      title: "Biệt Danh",
      desc: "Siêu nhân tiến hóa",
    },
  ];

  return (
    <Grid container direction="column">
      <Grid item>
        <MyBigBox
          describeInput={true}
          timeInput={false}
          labelDescInput="Giới thiệu về bản thân"
          descInputPlaceholder="Viết vài dòng giới thiệu bản thân"
          title="GIỚI THIỆU VỀ BẢN THÂN"
          labelAddInput="Viết một số chi tiết về bản thân bạn"
        />
        <ContactInfo
          label="Đơn giản, dễ tính, vui vẻ, chân thành và nhiệt tình"
          iconPublic={true}
        />
      </Grid>
      <Grid item>
        <MyBigBox
          title="CÁC TÊN KHÁC"
          labelAddInput="Thêm biệt danh, tên khai sinh..."
          dataInputField={otherName}
          listData={levelSchoolList}
        />
      </Grid>
      <Grid item>
        <MyBigBox
          labelDescInput="Trích dẫn yêu thích"
          describeInput={true}
          timeInput={false}
          title="TRÍCH DẪN YÊU THÍCH"
          descInputPlaceholder="Viết một cái gì đó?"
          labelAddInput="Thêm trích dẫn yêu thích của bạn"
        />
      </Grid>
    </Grid>
  );
};

export default Details;
