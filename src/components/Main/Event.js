import React from "react";
import { Grid } from "@material-ui/core";
import MyBigBox from "../common/MyBigBox";

const Event = () => {
  const fakeData1 = [
    {
      title: "2016",
      desc: "Bắt đầu đi học",
    },
    {
      title: "2017",
      desc: "Tiếp tục đi học",
    },
    {
      title: "Đến nay",
      desc: "Vẫn đi học",
    },
  ];

  return (
    <Grid container direction="column">
      <MyBigBox
        listData={fakeData1}
        title="Sự kiện trong đời"
        labelAddInput="Thêm sự kiện trong đời"
      />
    </Grid>
  );
};

export default Event;
