import React, { useState } from "react";
import { Grid, Typography } from "@material-ui/core";
import PublicIcon from "@material-ui/icons/Public";
import ContactInfo from "../common/ContactInfo";
import MyList from "../common/MyList";
import LockIcon from "@material-ui/icons/Lock";
import ContactForm from "../common/ContactForm";

const MainInfo = () => {
  const dataContact = [
    {
      label: "Di động",
      listInfo: [
        {
          info: "0123456789",
          icon: <PublicIcon />,
        },
        {
          info: "0123412544",
          icon: <LockIcon />,
        },
      ],
      form: (
        <ContactForm
          title="Di động"
          listInfo={[
            { id: 1, info: "info1" },
            { id: 2, info: "info2" },
          ]}
          isShowForm={true}
          titleBtn="Thêm số điện thoại"
          placeholder="Nhập số điện thoại"
        />
      ),
    },
    {
      label: "Địa chỉ",
      listInfo: [
        {
          info: "Ho Chi Minh City, Vietnam",
        },
      ],
      iconPublic: true,
    },
    {
      label: "Email",
      listInfo: [
        {
          info: "abc@gmail.com",
          icon: <LockIcon />,
        },
        {
          info: "def@yahoo.com",
          icon: <PublicIcon />,
        },
      ],
      form: (
        <ContactForm
          title="Email"
          listInfo={[
            { id: 1, info: "mail1" },
            { id: 1, info: "mail2" },
          ]}
          titleBtn="Thêm địa chỉ"
          placeholder="Nhập địa chỉ Email"
        />
      ),
    },
    {
      label: "Hahalolo URL",
      listInfo: [
        {
          info: "https://hahalolo.com/tienle",
        },
      ],
    },
  ];

  const dataInfoBasic = [
    {
      label: "Ngày sinh",
      listInfo: [
        {
          info: "25/10/1993",
        },
      ],
    },
    {
      label: "Giới tính",
      iconPublic: true,
      listInfo: [
        {
          info: "Nam",
        },
      ],
    },
  ];

  return (
    <Grid container direction="column">
      <div>
        <Typography style={{ marginBottom: "1rem", fontWeight: "500" }}>
          THÔNG TIN LIÊN HỆ
        </Typography>
        <MyList
          data={dataContact}
          render={(item, index) => (
            <ContactInfo
              key={index}
              title="Thong tin lien he"
              label={item.label}
              listInfo={item.listInfo}
              iconPublic={item.iconPublic}
              form={item.form}
            />
          )}
        />
      </div>
      <div>
        <Typography style={{ marginBottom: "1rem", fontWeight: "500" }}>
          THÔNG TIN CƠ BẢN
        </Typography>
        <MyList
          data={dataInfoBasic}
          render={(item, index) => (
            <ContactInfo
              key={index}
              title="Thong tin lien he"
              label={item.label}
              listInfo={item.listInfo}
              iconPublic={item.iconPublic}
            />
          )}
        />
      </div>
    </Grid>
  );
};

export default MainInfo;
