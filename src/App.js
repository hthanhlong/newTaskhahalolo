import React, { useState } from "react";
import Menu from "./components/Menu";
import Info from "./components/Info";
//
import "./App.css";
import Content from "./components/Main/Content";
import WorkAndLife from "./components/Main/WorkAndLife";
import OldPlaced from "./components/Main/OldPlaced";
import MainInfo from "./components/Main/MainInfo";
import Family from "./components/Main/Family";
import Event from "./components/Main/Event";
import Details from "./components/Main/Details";
import CssBaseline from "@material-ui/core/CssBaseline";

function App() {
  const [isChecked, setIsChecked] = useState(1);

  const main = [
    {
      id: 1,
      component: <Content />,
    },
    {
      id: 2,
      component: <WorkAndLife />,
    },
    {
      id: 3,
      component: <OldPlaced />,
    },
    {
      id: 4,
      component: <MainInfo />,
    },
    {
      id: 5,
      component: <Family />,
    },
    {
      id: 6,
      component: <Details />,
    },
    {
      id: 7,
      component: <Event />,
    },
  ];

  const handleSelectedFromApp = (item) => {
    const id = item.id;
    if (id) setIsChecked(id);
  };

  return (
    <>
      <CssBaseline />
      <div className="about fluid-container">
        <div className="row">
          <div className="col-12 about_title">Giới Thiệu (Version 2)</div>
        </div>
        <div className="wrapperbody row">
          <div className="firstcol col-12 col-sm-12 col-md-6 col-lg-3">
            <Menu handleSelectedFromApp={handleSelectedFromApp} />
          </div>
          <div className="col-12 col-sm-12 col-md-6 col-lg-9">
            {main &&
              main.map((item, index) =>
                item.id === isChecked ? (
                  <div key={index}>{item.component}</div>
                ) : (
                  ""
                )
              )}
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
